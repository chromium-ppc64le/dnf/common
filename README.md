# Repo Common Files

This repository holds common files for for the
[`chromium`](https://gitlab.com/chromium-ppc64le/dnf/chromium) and
[`chromium-testing`](https://gitlab.com/chromium-ppc64le/dnf/chromium-testing)
repos.

