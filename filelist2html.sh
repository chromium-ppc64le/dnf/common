#!/bin/bash
#
# Copyright 2019 Colin Samples
#
# SPDX-License-Identifier: Apache-2.0
#

#
# This script recurses though a directory tree given as an argument, creating
# in each directory an index.html file that lists the name, size and
# modification time of each file.
#
# Usage:
#   ./filelist2html.sh <DIR>
#   DIR - directory to recurse through
#

for dir in $(find $1 -type d); do
{
    echo "<html>"
    echo "<head><title>Index of $dir</title>"
    echo "<body>"
    echo "<h1>Index of $dir</h1><hr><pre><a href="../">../</a>"
    find $dir -maxdepth 1 -mindepth 1 -printf "%P|%Td-%Tb-%TY %TH:%TM|%s\0" \
        | sort -k1 -z | awk -F\| 'BEGIN { RS = "\0" }
    {
        if (length($1) > 50)
            filename = substr($1, 1, 43)"..>"
        else
            filename = $1
        printf "<a href=\"%s\">%-50s %s %10s\n", $1, filename"</a>", $2, $3
    }'
    echo "</pre><hr></body>"
    echo "</html>"
} > $dir/index.html
done

